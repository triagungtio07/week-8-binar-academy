const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
require("dotenv").config();
const app = express();

const port = process.env.NODE_PORT;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

function renameFile(oldnames) {
  const [filename, extensions] = oldnames.split(".");
  return `${filename}.${extensions}`;
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    cb(null, renameFile(file.originalname));
  },
});
const uploads = multer({ storage: storage });

app.post("/register", uploads.single("file"), (req, res) =>
  res.send("succes uploads")
);

app.listen(port, () => console.log(`this app running on port ${port}`));
