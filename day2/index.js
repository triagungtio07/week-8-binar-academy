const express = require("express");
const app = express();

app.get("/products", (req, res) => res.send("this is products"));

app.listen(3000, () => console.log("this app running on port 3000"));

module.exports = app;
