const app = require("../index");
const request = require("supertest");

test("routes / products should return message this is products", () => {
  return request(app).get("/products").expect(200).expect("this is products");
});

module.exports = app;
